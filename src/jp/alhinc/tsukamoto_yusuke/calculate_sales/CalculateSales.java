package jp.alhinc.tsukamoto_yusuke.calculate_sales;

//各店舗ごとの売上額を集計する
//そのために支店定義ファイルと売上ファイルを使い支店別集計ファイルを作る
//支店別集計ファイルを作るには支店コード、支店名、売上額の三要素を合わせる必要がある
//支店定義ファイルと売上ファイルの共通点は支店コードなので
//支店定義ファイル（支店コード　＋　支店名　）と売上ファイル（支店コード　＋　売上額）の二つのMapを使えば支店別集計ファイル表示できる
//↑branch.lst									↑8桁.rcd													↑branch.out

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		if( args.length != 1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		System.out.println("ここにあるファイルを開きます =>" + args[0]);

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		BufferedReader br = null;

		if( !readFile( branchNames , branchSales , args[0] , "branch.lst" , "支店","^[0-9]{3}") ) {
			return ;
		}

		Map<String, String> productsNames = new HashMap<>();
		Map<String, Long> productsSales = new HashMap<>();
		if( !readFile( productsNames , productsSales , args[0] , "commodity.lst" , "商品","^[0-9A-Z]{8}") ) {
			return ;
		}


		//売上ファイル読み込み処理		branchSalesで設定した0Lからドンドン値を足して集計していく処理

		File[] files = new File(args[0]).listFiles();				//売上ファイルの抽出	//File[] files = new File(args[0]).listFiles();		File file = new File(args[0]);
		//ファイルの命名(8桁.rcd)は決まっているが数がわからない→売上ファイルだけを集める処理(lstを作る)
		List<File> rcdFiles = new ArrayList<>();					//まずは空のファイルを作る	→	List<File>

		for(int i = 0; i < files.length ; i++) {					//なぜ,lengthか→配列の要素数を取りたい場合はlength
			String fileName = files[i].getName();					//リストの場合はsize	File[]→配列,files→リスト

			//ファイルかディレクトリかどうか		//ここでfileNameが0から9の数字から始まる8桁の数字でrcdで終わるかどうかを調べる
			if( files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				//(ミス)fileNameがrcdで終わるかどうかを調べる
				rcdFiles.add(files[i]);																						//if(fileName.matches(".+rcd$")) {															//System.out.println("rcdで終わります")
			}																												// fileNameが0から9の数字から始まる8桁の数字どうかを調べる
		}																													//if(fileName.matches("^[0-9]{8}+$")) {
		//System.out.println("0から9の数字から始まる8桁の数字です");
		//連番チェック
		Collections.sort(rcdFiles);
		//なぜ,rcdFiles.size() - 1の-1するか → このリストはiは0～4の要素しかない。だからget(i + 1)でget(5)となり要素数をこえエラーとなるから
		for( int i = 0 ; i < rcdFiles.size() - 1 ; i++ ) {											//解説 i = 1 の時
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));				//File file1(例) = rcdFiles.get(i);		file1に001.rcdのファイルがとれた
			//										001		rcd			0～8						//File file2	= rcdFiles.get(i + 1);	file2に002.rcdのファイルがとれた
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));			//次に数字の部分を取る
			if((latter - former) != 1) {															//String file1Name = file1.getName();  → 001.rcd
				System.out.println("売上ファイル名が連番になっていません");							//String file2Name = file2.getName();  → 002.rcd
				return;																			//これで文字列が取れた。次に先頭の8文字をとる
			}																						//Strin gfile1Number = file1Name.substring(0,8); → 001
		}																							//String file2Number = file2Name.substring(0,8); → 002
		//数字を取る
		//int former = Integer.parseInt(file1Number); → 1
		for(int i = 0; i < rcdFiles.size(); i++) {													//int latter = Integer.parseInt(file2Number); → 2
			try {																					//次に連番かチェック (if文)

				br = new BufferedReader(new FileReader(rcdFiles.get(i)));	//売上ファイルを開いている
				//rcdFilesに入っているのは売上ファイルです。ただし売上ファイルは支店定義ファイルと違って複数存在します。
				//そのため一旦Listに保持しています。今度はそのファイル一つ一つを順番に開いて中身を見る必要があります。
				//「ファイルを一つ一つ順番に開く」ためにrcdFilesに対してfor文を使ってあげて、rcdFiles.get(i)でファイルを取り出しているイメージです。
				ArrayList<String> fileContents = new ArrayList<>();		//１行読み込んでリスト(fileContents)へつめていく
				String line = "";											//lineという文字列作成
				while((line = br.readLine()) != null) {
					fileContents.add(line);
				}

				String fileName = rcdFiles.get(i).getName();				//rcdFilesに入っているのは売上ファイルで、そのファイル名をgetNameしている

				//行数チェック
				//6/9売上集計課題で追加 2→3
				if( fileContents.size() != 3) {
					System.out.println( fileName + "のフォーマットが不正です");
					return;
				}

				//	売上金額が数字かどうか
				if( !fileContents.get(2).matches("^[0-9]+$") ) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}

				//支店コードの存在確認
				if( !branchNames.containsKey(fileContents.get(0)) ) {		//containsKey → 指定したkeyが存在するか確認する(Mapのkeyを検索する処理) → Map.containsKey(検索するkey)	//ミス if( items[0] != fileContents.get(0) ) {
					System.out.println( fileName + "の支店コードが不正です");
					return;
				}


				//商品コードの存在確認
				if( !productsNames.containsKey(fileContents.get(1)) ) {		//containsKey → 指定したkeyが存在するか確認する(Mapのkeyを検索する処理) → Map.containsKey(検索するkey)	//ミス if( items[0] != fileContents.get(0) ) {
					System.out.println( fileName + "の商品コードが不正です");
					return;
				}

				//支店コード
				String branchCode = fileContents.get(0);					//行を保持するための変数		Listから取り出す

				//売上金額の加算
				long fileSale = Long.parseLong(fileContents.get(2));		//売上金額→文字列ではなく数字	String	→	Long
				Long BranchSaleAmount = branchSales.get(branchCode) + fileSale;	//										↑parseLong
				//	Long saleAmount = 0L + get(1)でとった値

				if( BranchSaleAmount > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(branchCode, BranchSaleAmount);					//putで支店コード、合計金額

				//商品コード
				String productsCode = fileContents.get(1);					//行を保持するための変数		Listから取り出す
				Long productSaleAmount = productsSales.get(productsCode) + fileSale;	//										↑parseLong
				//	Long saleAmount = 0L + get(1)でとった値

				if( productSaleAmount > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				productsSales.put(productsCode, productSaleAmount);

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}
				}
			}
		}

		//writeFile( branchNames , branchSales , args[0] , "branch.out" );	これを書くと二回読み込んでることになる。またif文の後はfalseのときはreturnすることを書いている
		if( !writeFile( branchNames , branchSales , args[0] , "branch.out","支店","^[0-9]{3}" ) ) {
			return;
		}

		if( !writeFile( productsNames , productsSales , args[0] , "commodity.out","商品","^[0-9A-Z]{8}"
				) ) {
			return;
		}
		//支店別集計ファイル書き込み処理
	}


	//売上集計ファイル



	private static boolean writeFile(Map<String, String> Names , Map<String, Long> Sales , String filePath , String fileName, String shopProduct,String cord) {

		BufferedWriter bw = null;			//	ファイルに書き込むためにはBufferedWriteが必要ですが、そのためにはFileWriterが必要であり、
		//	FileWriterrを作るためにはFileが必要となるため、インスタンス生成のタイミングでさらにインスタンスを生成しているイメージ
		try {
			File file = new File(filePath, fileName);
			bw = new BufferedWriter(new FileWriter(file));

			for (String key : Names.keySet() ) {
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key));	//( 商品コード , 商品名 , 売上金額)	がcommodity.outというファイル名でエクスプローラーに追加される
				bw.newLine();	//このコードで改行できる
			}

		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			System.out.println("予期せぬエラーが発生しました。");
			return false;

		}finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean readFile( Map<String, String> Names , Map<String, Long> Sales , String filePath , String fileName,String shopProduct,String cord) {

		BufferedReader br = null;
		try {	//通常実行される文
			//ファイルのパスを指定する

			File file = new File(filePath , fileName);

			if( !file.exists()) {
				System.out.println( shopProduct + "定義ファイルが存在しません");
				return false;
			}																//	ファイルを読み込むためにはBufferedReaderが必要ですが、そのためにはFileReaderが必要であり、

			br = new BufferedReader(new FileReader(file));				//	FileReaderを作るためにはFileが必要となるため、インスタンス生成のタイミングでさらにインスタンスを生成しているイメージ

			//	BufferedReaderクラスのreadlineメソッドを使って１行ずつ読み込み表示する

			FileReader fr = new FileReader(file);

			//	BufferedReader br = new BufferedReader(fr);

			br = new BufferedReader(fr);		//brというインスタンスを作っている。←支店定義ファイルを開いてる

			String line;	//String : 文字列 ( int,integer,long : 整数 )
			while(	(line = br.readLine())	!=	null	)	{

				String[] items = line.split(",");
				// 変数	分割する		「支店コード.支店名」の一つのまとまりを分ける

				if (!items[0].matches( cord ) || (items.length != 2)) {
					System.out.println( shopProduct + "定義ファイルのフォーマットが不正です");
					return false;
				}

				Names.put(items[0], items[1]);		// items[0]番目に支店コード　items[1]番目に支店名
				Sales.put(items[0], 0L);				// items[0]番目に支店コード  売上の最初は0なので0
				//							またLong型で値を追加したいときはLを
			}

		} catch(IOException e) {							// } catch(例外クラス　変数名(引数)) {  ← 例外発生時に実行される文
			System.out.println("エラーが発生しました。");
			return false;
		}finally {											// 例外の有無に関わらず、最後に必ず実行される処理
			if(br != null) {								// ファイルを開いているかの確認
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");

					return false;		// returnは戻り値を返す時に使う

				}
			}
		}
		return true;
	}

}